# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome
- Nom, Prénom, email: SALAMI Aridath aridath.salami.etu@univ-lille.fr

- Nom, Prénom, email: DIAKITE Aboubakar aboubakarsiriki.diakite.etu@univ-lille.fr


## Question 1

Oui,le processus peut ecrire car le groupe ubuntu auquel toto appartient a le droit d'ecriture sur le fichier. 

## Question 2

a - Pour un repertoire le caractère x signifie que nous pouvons accèder a ce repertoire sinon s'il n'''est pas present c'est à dire si nous avons par exemple un repertoire ``myRepertory`` avec les droits suivons drw-rwxr-x, alors si nous essayons de rentrer dans ce repertoire nous aurons un Permission denied.  

b - Nous constatons que l'utilisateur toto ne peut pas accéder au repertoire mydir car le groupe auquel il appartient ubuntu n'a plus le droit d'execution sur ce repertoire.Donc nous avons une Permission denied. 

c - Lorsque nous faisons ls -al mydir avec l'utilisateur ubuntu nous n'avons pas les informations(droit d'accès,groupes,date de creations ...)  relatifs aux fichiers contenu dans le repertoire mydir. nous avons  juste les noms des fichiers que mydir contient.Pour les mêmes raisons que la sous question b.


## Question 3

- les valeurs des differents ids:
EUID -> 1001,
EGID -> 1001
RUID -> 1001,
RGID -> 1001;

Le processus n'arrive pas a ouvrir le fichier mydir/data.txt en lecture car il se lance avec l'EUID de toto et celui-ci n'a pas le droit de lecture sur ce fichier ce processus appartient à l'utilisateur ubuntu. et nous pouvons voir que l'EUID de toto et ubuntu sont differents.

- les valeurs des differents ids apres le flag set-user-id:

EUID -> 1000,
EGID -> 1001,
RUID -> 1001,
RGID -> 1001;

Après l'activation du flag set-user-id sur notre executable suid  nous constatons que le fichiers peut à present s'ouvrir en lecture. Céla est possible car le flag a permis de lancer notre programme avec l'EUID de ubuntu. et nous pouvons voir que dans toto l'EUID =1000 comme dans ubuntu.


## Question 4

Les valeurs des ids:
- Effective user id of the current process: 1001
- Effective group id of the current process: 1001

le fichier data.txt ne s'ouvre pas en lecture même apres l'activation du flag nous avons ce message d'erreur:

Traceback (most recent call last):
  File "suid.py", line 9, in <module>
    f = open("mydir/data.txt","r")
PermissionError: [Errno 13] Permission denied: 'mydir/data.txt'


## Question 5

 	
1- La commande chfn modifie le nom complet de l'utilisateur, le numéro de la chambre du bureau, le numéro de téléphone du bureau et le numéro de téléphone du domicile pour le compte d'un utilisateur.
2 - ls -al /usr/bin/chfn
	-rwsr-xr-x 1 root root 85064 May 28  2020 /usr/bin/chfn 

	La commande chfn etant un precessus privilègié,donc tout utilisateur peut l'executer sans l'autorisation de l'administrateur.


## Question 6

- Le mot de passe est stockés dans le fichier `/etc/shadow` dans lorsqu'on fait la commande `ls -al /etc/shadow`  dans notre user ubuntu nous le resultat suivant:
	`-rw-r----- 1 root shadow 1156 Jan 25 15:15 /etc/shadow`

- D'abord parceque si le mot de passe etait stocké dans `/etc/passwd` alors il allait etre visible par tout le monde.
- Donc ils sont donc stockés dans le fichier `/etc/shadow` car ce fichier ne peut etre lu que par l'utiisateur `root`

## Question 7

Mettre les scripts bash dans le repertoire *question7*.

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








