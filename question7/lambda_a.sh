#!/bin/bash

#effacer fichier
effacer(){

	rm -f $1
}
#renommer fichier
renommer(){
	mv $1 $1.new
}

#modifier

modifier(){

	echo "lambda_a" >>$1
}
#determiner s'il s'agit d'un doddier ou un fichier et afficher le contenu des dossiers
faire_le_tree(){
	for arg in $*
	do
		if [ $arg!=$2 ]
		then
			if [ -f $2$arg ]
			then
				echo "il s'agit d'un fichier :$arg"
				$1 $2$arg
			fi
			if [ -d $2$arg ]
			then
				echo "il s'agit d'un dossier :$arg"
				faire_le_tree $1 $2$arg/ $(ls $2$arg)
			fi

		fi

	done

}
##les fichiers dont l'utilisateur n'est paas propriétaire
proprietaire_different(){
	if [ $# != 1 ]
	then
		
		if [ $4 != "lambda_b" ]
		then
			renommer $1
			effacer $1
		fi
		
	else
		proprietaire_different $1 $(ls -al $1)
	fi
}
echo "peuvent lire tous les fichiers et sous-répertoires contenus dans dir_a et dir_c ;"
echo "----------------------------"
faire_le_tree "cat" "dir_a/" $(ls dir_a/)
faire_le_tree "cat" "dir_c/" $(ls dir_c/)
echo "----------------------------"

echo "peuvent lire, mais ne peuvent pas modifier les fichiers dans dir_c, ni les renommer, ni les effacer,ni créer des nouveaux fichiers."
echo "----------------------------"

faire_le_tree "modifier" "dir_c/" $(ls dir_c/)
faire_le_tree "renommer" "dir_c/" $(ls dir_c/)
faire_le_tree "effacer" "dir_c/" $(ls dir_c/)
touch dir_c/lambd_a.txt $(ls dir_c/)
echo "----------------------------"

echo "peuvent modifier tous les fichiers contenus dans l’arborescence à partir de dir_a, et peuvent créer de nouveaux fichiers et répertoires dans dir_a ;"
echo "----------------------------"
faire_le_tree "modifier" "dir_a/" $(ls dir_a/)
touch dir_a/lambd_a.txt
mkdir dir_a/lambd_a $(ls dir_a/)

echo "----------------------------"


echo "n’ont pas le droit d’effacer, ni de renommer, des fichiers dans dir_a qui ne leur appartiennent pas ;"
echo "----------------------------"
faire_le_tree "proprietaire_different"  "dir_a" $(ls dir_a/)
echo "----------------------------"

echo  "ne peuvent pas ni lire, ni modifier, ni effacer les fichiers dans dir_b, et ne peuvent pas créer des nouveaux fichiers dans dir_b."
echo "----------------------------"
faire_le_tree "cat" "dir_b/" $(ls dir_b/)
faire_le_tree "modifier" "dir_b/" $(ls dir_b/)
faire_le_tree "effacer" "dir_b/" $(ls dir_b/)
touch dir_b/lambd_a.txt $(ls dir_b/)
echo "----------------------------"

