#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pwd.h>




int rmg(char *filename) {
	struct stat s;
  	struct passwd *pw;
  	char password[15];
  	int gid = getgid();
  
  
  	if(stat(filename, &s) == -1) {
    		perror("un probleme avec stat");
    		exit(EXIT_FAILURE);
  	}
  
  	if(s.st_gid != gid){
    		perror("accès refusé\n");
    		exit(1);
  	}

  	printf("paassword: \n" );
  	fgets(password, 15, stdin);
  	pw = getpwuid(getuid());
 
  	if(checkpass("passwd",pw->pw_name,password)==1) {
    		if(remove(filename) == 0) {
      			printf("\n#### fichier supprimé ###\n" );
    		} else {
      			printf("FATAL: erreur suppression du fichier \n");
    		}
  	} else { 
    		exit(1);
  	}

  	exit(0);
}


int main(int argc, char  *argv[]) {
  	rmg(argv[1]);
  	return 0;
}
