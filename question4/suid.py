import os 
  
ruid, euid, suid = os.getresuid() 
rgid, egid, sgid =os.getresgid()
  

print("Effective user id of the current process:", euid) 
print("Effective group id of the current process:", egid) 
